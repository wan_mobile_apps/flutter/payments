import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// SquareAction class.
/// this class represents a widget
class SquareAction extends StatelessWidget {
  final String text;
  final IconData iconData;
  final Color contentColor;
  final Color backgroundColor;
  final VoidCallback onPressed;

  /// Contructor method
  /// [text] is required.
  /// [iconData] is required.
  /// [contentColor] is optional. if the value is not present the constructor
  /// will initialize it.
  /// [backgroundColor] is optional. if the value is not present the constructor
  /// will initialize it.
  /// [onPressed] is required.
  SquareAction(
      {required this.text,
      required this.iconData,
      this.contentColor: const Color(0xFFf8f9fa),
      this.backgroundColor: const Color(0xFF343A40),
      required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(4),
      width: 85.0,
      height: 85.0,
      child: Material(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(10.0),
        child: InkWell(
          borderRadius: BorderRadius.circular(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                  padding: EdgeInsets.only(
                      top: 6.0, right: 4.0, bottom: 4.0, left: 4.0),
                  child: Icon(
                    iconData,
                    color: contentColor,
                    size: 30,
                  )),
              Padding(
                padding: EdgeInsets.only(right: 4.0, bottom: 4.0, left: 4.0),
                child: RichText(
                    text: TextSpan(
                        text: text,
                        style: TextStyle(
                          fontSize: 13,
                          color: contentColor,
                        ))),
              )
            ],
          ),
          onTap: onPressed,
        ),
      ),
    );
  }
}
