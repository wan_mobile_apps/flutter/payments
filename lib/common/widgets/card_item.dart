import 'package:flutter/material.dart';
import 'package:payments/common/utils/colors.dart';


/// CardItem class
/// This class represents a widget.
class CardItem extends StatelessWidget{
  
  final String lastFourNumers;
  final String brand;
  final bool infoOnTheRight;

  /// Constructor method.
  /// [lastFourNumers] is required.
  /// [brand] is required.
  /// [infoOnTheRight] is optional. the constuctor sets a default value.
  CardItem({
    required this.lastFourNumers,
    required this.brand,
    this.infoOnTheRight : true
  });

  

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if(!infoOnTheRight)
          getInfoCard(),
        Container(
          margin: EdgeInsets.all(6.0),
          width: 48,
          height: 75,
          decoration: BoxDecoration(
            color: Color(0xFF343A40),
            borderRadius: BorderRadius.circular(5.0)
          )),
        if(infoOnTheRight)
          getInfoCard(),
      ],
    );
  }

  getInfoCard() => Padding(
    padding: EdgeInsets.only(top: 9.0),
    child: Column(
      crossAxisAlignment: infoOnTheRight 
        ? CrossAxisAlignment.start : CrossAxisAlignment.end,
      children: [
        RichText(
          text: TextSpan(
            text: '$brand',
            style: TextStyle(
              fontSize: 9.0))),
        RichText(
          text: TextSpan(
            text: '$lastFourNumers',
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.w900,
              color: AppColors.grey_first_tone,
              fontFamily: 'OdibeeSans'))),
      ],
    ));
}