import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:payments/common/utils/colors.dart';

/// AvailableBalance class
/// This class represents a widget.
class AvailableBalance extends StatelessWidget{
  
  final Color textColor;
  final Color amountColor;
  final String amount;
  final CrossAxisAlignment alignment;


  /// Contructor method.
  /// [textColor] is optional. The constructor method sets default value.
  /// [amountColor] is optional. The constructor method sets default value.
  /// [amount] is required.
  /// [alignment] is optional. The constructor method sets default value as [CrossAxisAlignment.end]
  AvailableBalance({
    this.textColor: AppColors.grey_first_tone,
    this.amountColor: AppColors.white_first_tone,
    required this.amount,
    this.alignment: CrossAxisAlignment.end
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: alignment,
      children: [
        Padding(
          padding: EdgeInsets.all(4.0),
          child: RichText(
            text: TextSpan(
              text: AppLocalizations.of(context)!.availableBalance,
              style: TextStyle(color: textColor),
            ))),
        Padding(
          padding: EdgeInsets.all(4.0),
          child: RichText(
            text: TextSpan(
                text: amount,
                style: TextStyle(
                  color: amountColor,
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'OdibeeSans'
            ))))
      ],
    );

  }

}