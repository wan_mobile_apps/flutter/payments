import 'package:flutter/material.dart';
import 'package:payments/common/widgets/square_action.dart';

/// ActionList
/// This class represent a widget.
class ActionList extends StatelessWidget {
  final List<SquareAction> children;

  /// Constructor method.
  /// [children] is required.
  ActionList({required this.children});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(children: children),
    );
  }
}
