import 'package:flutter/material.dart';

class AppColors {
  static const black_first_tone = Color(0xff343A40);
  static const black_second_tone = Color(0xff212529);
  static const black_third_tone = Color(0xff000000);

  static const grey_first_tone = Color(0xffadb5bd);
  static const grey_second_tone = Color(0xff6c757d);

  static const white_first_tone = Color(0xfff8f9fa);
  static const white_second_tone = Color(0xffe9ecef);
  static const white_third_tone = Color(0xffdee2e6);
}